$(document).ready(function(){
	
	$('.banner_slider').owlCarousel({	
	    loop:true,
	    autoplay: true,
		autoplayTimeout: 3000,    
	    nav:false,
	    dots:false,
		responsive:{
			0:{
				items:1
			},
		}
	});

	$('.category_slider').owlCarousel({	
		    loop:true,
		    stagePadding: 80,
		    autoplay: false,
			autoplayTimeout: 3000,    
		    nav:false,
		    margin:30,
		    dots:false,
			responsive:{
				0:{
					stagePadding: 30,
					items:3,
					margin:25,
					loop: false,
				},
				600:{
					items:4
				},				
				700:{
					items:5
				},
				800:{
					items:6
				},
				1000:{
		            items:7,            
		        },	        
		        1200:{
		            items:8,            
		        }
			}
		});

	$('.popular_post').owlCarousel({	
	    loop:true,	    
	    autoplay: false,
		autoplayTimeout: 3000,    
	    nav:false,
	    margin:30,
	    dots:false,
		responsive:{
			0:{
				items:1,
				stagePadding: 0,
				margin:15,
				autoplay: true,
			},
			600:{
				items:2
			},
			1000:{
	            items:2,            
	        },	        
	        1200:{
	            items:2,            
	        }
		}
	});

	$('.recent_post').owlCarousel({	
		    loop:false,
		    stagePadding: 40,
		    autoplay: false,
			autoplayTimeout: 3000,    
		    nav:false,
		    margin:15,
		    dots:false,
			responsive:{
				0:{		
					loop:true,								
					items:1,
					stagePadding: 60,
					margin:15,
				},
				500:{
					items:2,
				},
				600:{
					items:3,
				},
				1000:{
		            items:4,            
		        },	        
		        1200:{
		            items:5,            
		        }
			}
		});



});